import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import appReducer from './appReducer'
import boot from './boot'

export default function configureStore() {
  const isDev = process.env.NODE_ENV === 'development'
  const enhance = compose(
    applyMiddleware(thunk),
    window.devToolsExtension && isDev ? window.devToolsExtension() : f => f
  )
  const store = createStore(appReducer, enhance)
  if (isDev) {
    if (module.hot) {
      // Enable Webpack hot module replacement for reducers
      module.hot.accept('./appReducer', () => {
        const nextRootReducer = require('./appReducer')
        store.replaceReducer(nextRootReducer)
      })
    }
  }

  store.dispatch(boot())
  return store
}
