import { forEachObjIndexed } from 'ramda'
import { hmset } from '../core/hash/actions'

export const data = {
  i18n: {
    es: {
      title: 'Esto es el título',
      button: { label: 'Esto es una etiqueta' }
    },
    en: { title: 'This is a title', button: { label: 'This is a label' } }
  },
  setting: {
    es: { locale: 'es', fallback: 'en' },
    uk: { locale: 'en' }
  },
  theme: {
    default: { background: '#a03' },
    black: { background: '#000' },
    light: { background: '#eee' }
  }
}

const boot = () => dispatch => {
  forEachObjIndexed((value, key) => dispatch(hmset(key, value)), data)
}

window.localStorage.setItem('i18n', JSON.stringify(data.i18n))
window.sessionStorage.setItem('i18n', JSON.stringify(data.i18n))

export default boot
