import { combineReducers } from 'redux'
import hash from './core/hash'

export default combineReducers({
  hash
})
