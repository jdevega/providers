import { HSET, HDEL, HMSET, HINCRBY, HTOGGLE } from './actionTypes'
import { curry } from 'ramda'

/**
 * Sets a single value in a hash, specified by
 * domain and key
 * @param {String} domain
 * @param {String} keyPath
 * @param {any} value
 * @return {Object}
 */
export const hset = curry((domain, keyPath, value) => ({
  type: HSET,
  payload: {
    path: [domain, keyPath],
    value
  }
}))

export const hdel = curry((domain, keyPath) => ({
  type: HDEL,
  payload: {
    path: [domain, keyPath]
  }
}))

/**
 * Merges a Javascript object in an existing hash
 * @param {String} domain
 * @param {Object} map
 * @return {Object}
 */
export const hmset = curry((domain, map) => ({
  type: HMSET,
  payload: {
    path: [domain],
    value: map
  }
}))

export const hincrby = curry((domain, key, delta) => ({
  type: HINCRBY,
  payload: {
    path: [domain, key],
    value: delta
  }
}))

/*
Toggles a Boolean key in a hash. If key is not present, it will assumed to
be false, so htoggle() will cause it to be true.
*/

export const htoggle = curry((domain, key) => ({
  type: HTOGGLE,
  payload: {
    path: [domain, key]
  }
}))
