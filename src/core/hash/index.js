// import dotProp from 'dot-prop-immutable'
import {
  set,
  lensPath,
  dissocPath,
  over,
  mergeDeepLeft,
  view,
  compose,
  defaultTo,
  add,
  not,
  pipe
} from 'ramda'
import { buildReducer } from '../lib/buildReducer'
import * as ActionTypes from './actionTypes'
export * from './actions'
export * from './selectors'

const reducer = buildReducer(
  {},
  {
    [ActionTypes.HSET]: (state, action) => {
      const { value, path } = action.payload
      return set(lensPath(path), value, state)
    },
    [ActionTypes.HDEL]: (state, action) => {
      const { path } = action.payload
      return dissocPath(path, state)
    },
    [ActionTypes.HMSET]: (state, action) => {
      const { value, path } = action.payload
      return over(
        lensPath(path),
        pipe(defaultTo({}), mergeDeepLeft(value)),
        state
      )
    },
    [ActionTypes.HINCRBY]: (state, action) => {
      const { value, path } = action.payload
      if (view(lensPath(path), state)) {
        return over(lensPath(path), compose(defaultTo(1), add(value)), state)
      }
      return set(lensPath(path), value, state)
    },
    [ActionTypes.HTOGGLE]: (state, action) => {
      const { path } = action.payload
      return over(lensPath(path), not, state)
    }
  }
)

export default reducer
