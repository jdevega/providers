import {
  curry,
  keys,
  propOr,
  split,
  join,
  flatten,
  path,
  complement,
  length,
  isNil
} from 'ramda'

const EMPTY_OBJECT = {}
const EMPTY_ARRAY = []

const root = propOr(EMPTY_OBJECT, 'hash')
const pathFor = chunks =>
  split('.', join('.', flatten(EMPTY_ARRAY.concat(chunks))))

/**
 * Returns a single property from a hash
 * @param {Object} state Redux state
 * @param {String} hash  Hash key
 * @param {String} key   Key string or key path (xx.yy.zzz)
 */
export const hget = curry((hash, key, state) => {
  const result = path(pathFor([hash, key]), root(state))
  console.log(hash, key, state, result)
  return result
})

export const hgetAll = curry((hash, state) => path(pathFor(hash))(root(state)))

export const hkeys = curry((hash, state) => keys(hgetAll(hash, state)))

export const hlen = curry((hash, state) => length(hkeys(hash, state)))

export const hexists = curry((hash, key, state) =>
  complement(isNil)(hget(hash, key, state))
)
