import { withProps } from 'recompose'
import { createProvider, getDataFor } from '../lib'

const memoryDataProvider = ({ data }) =>
  withProps(props => ({
    data: getDataFor(props.domain)(data)
  }))

export default createProvider(memoryDataProvider)
