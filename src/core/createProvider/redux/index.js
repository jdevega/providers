import { compose } from 'recompose'
import { createProvider } from '../lib'

const reduxDataProvider = ({ connect, propName = 'data' }) => connect

export default createProvider(reduxDataProvider)
