import '../../../../../.jest/setup'
import React from 'react'
import createProvider from '../'
import { Provider as ReduxProvider, connect } from 'react-redux'
import { mount } from 'enzyme'
import { pathOr } from 'ramda'
describe('CreateReduxProvider', () => {
  const data = {
    a: {
      b: {
        c: 'This is a test'
      },
      d: 'Second level'
    },
    e: 'First level text'
  }
  const mockStore = {
    subscribe: jest.fn(),
    dispatch: jest.fn(),
    getState: () => ({
      hash: {
        data
      }
    })
  }

  class Mock extends React.Component {
    render() {
      return <div />
    }
  }

  const selector = (domain, state) =>
    pathOr({}, [].concat(domain.split('.')), state.hash.data)
  const mapStateToProps = (state, { domain }) => ({
    data: selector(domain, state)
  })
  const connectToI18n = connect(mapStateToProps)

  const { Provider, withProvider } = createProvider({
    connect: connectToI18n
  })

  it('Uses data from redux provider', () => {
    const DOMAIN = 'a'
    const MockWithProvider = withProvider('b')(Mock)
    const wrapper = mount(
      <ReduxProvider store={mockStore}>
        <Provider domain={DOMAIN}>
          <MockWithProvider />
        </Provider>
      </ReduxProvider>
    )
    expect(wrapper.exists()).toBe(true)
    const subject = wrapper.find(Provider).first()
    expect(subject.exists()).toBe(true)
    expect(subject.prop('domain')).toBe(DOMAIN)
  })
  describe('Provider and withProvider', () => {
    it('inject an object as `data` prop', () => {
      const DOMAIN = 'a'
      const MockWithProvider = withProvider('b')(Mock)
      const wrapper = mount(
        <ReduxProvider store={mockStore}>
          <Provider domain={DOMAIN}>
            <MockWithProvider />
          </Provider>
        </ReduxProvider>
      )

      const subject = wrapper.find(Mock)
      expect(subject.prop('data').b).toEqual(data.a.b)
    })

    it('inject multiple objects as `data` prop', () => {
      const DOMAIN = 'a'
      const MockWithProvider = withProvider(['b', 'd', 'e'])(Mock)
      const wrapper = mount(
        <ReduxProvider store={mockStore}>
          <Provider domain={DOMAIN}>
            <MockWithProvider />
          </Provider>
        </ReduxProvider>
      )

      const subject = wrapper.find(Mock)
      expect(subject.prop('data').b).toEqual(data.a.b)
      expect(subject.prop('data').d).toEqual(data.a.d)
      expect(subject.prop('data').e).toBeUndefined()
    })

    it('inject multiple objects as props named as key', () => {
      const DOMAIN = 'a'
      const MockWithProvider = withProvider({
        datab: 'b',
        datad: 'd',
        databc: 'b.c',
        datae: 'e'
      })(Mock)
      const wrapper = mount(
        <ReduxProvider store={mockStore}>
          <Provider domain={DOMAIN}>
            <MockWithProvider />
          </Provider>
        </ReduxProvider>
      )

      const subject = wrapper.find(Mock)
      expect(subject.prop('datab')).toEqual(data.a.b)
      expect(subject.prop('datad')).toEqual(data.a.d)
      expect(subject.prop('databc')).toEqual(data.a.b.c)
      expect(subject.prop('databe')).toBeUndefined()
    })
  })
})
