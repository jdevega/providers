import { createProviderWithStorage } from '../lib'

export default createProviderWithStorage(window.sessionStorage)
