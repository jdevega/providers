import '../../../../../.jest/setup'
import React from 'react'
import { mount } from 'enzyme'
import { createProviderWithStorage } from '../'

describe('createLocalStorageProvider', () => {
  class Mock extends React.Component {
    render() {
      return <div />
    }
  }

  const data = {
    a: {
      b: {
        c: 'This is a test'
      },
      d: 'Second level'
    },
    e: 'First level text'
  }

  const mockStorage = {
    i18n: JSON.stringify(data)
  }

  const { Provider, withProvider } = createProviderWithStorage(mockStorage)({
    storageKey: 'i18n'
  })
  describe('Provider', () => {
    it('inject domain as prop', () => {
      const DOMAIN = 'a'
      const wrapper = mount(
        <Provider domain={DOMAIN}>
          <Mock />
        </Provider>
      )

      expect(wrapper.exists()).toBe(true)
      const subject = wrapper.find(Provider).first()
      expect(subject.exists()).toBe(true)
      expect(subject.prop('domain')).toBe(DOMAIN)
    })
  })

  describe('Provider and withProvider', () => {
    it('inject an object as `data` prop', () => {
      const DOMAIN = 'a'
      const MockWithProvider = withProvider('b')(Mock)
      const wrapper = mount(
        <Provider domain={DOMAIN}>
          <MockWithProvider />
        </Provider>
      )

      const subject = wrapper.find(Mock)
      expect(subject.prop('data').b).toEqual(data.a.b)
    })

    it('inject multiple objects as `data` prop', () => {
      const DOMAIN = 'a'
      const MockWithProvider = withProvider(['b', 'd', 'e'])(Mock)
      const wrapper = mount(
        <Provider domain={DOMAIN}>
          <MockWithProvider />
        </Provider>
      )

      const subject = wrapper.find(Mock)
      expect(subject.prop('data').b).toEqual(data.a.b)
      expect(subject.prop('data').d).toEqual(data.a.d)
      expect(subject.prop('data').e).toBeUndefined()
    })

    it('inject multiple objects as props named as key', () => {
      const DOMAIN = 'a'
      const MockWithProvider = withProvider({
        datab: 'b',
        datad: 'd',
        databc: 'b.c',
        datae: 'e'
      })(Mock)
      const wrapper = mount(
        <Provider domain={DOMAIN}>
          <MockWithProvider />
        </Provider>
      )

      const subject = wrapper.find(Mock)
      expect(subject.prop('datab')).toEqual(data.a.b)
      expect(subject.prop('datad')).toEqual(data.a.d)
      expect(subject.prop('databc')).toEqual(data.a.b.c)
      expect(subject.prop('databe')).toBeUndefined()
    })
  })
})
