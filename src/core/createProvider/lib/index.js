import {
  compose,
  setDisplayName,
  setPropTypes,
  mapProps,
  withContext,
  getContext,
  withPropsOnChange,
  withProps
} from 'recompose'

import PropTypes from 'prop-types'
import {
  path,
  reduce,
  either,
  omit,
  propOr,
  mergeDeepRight,
  is,
  last,
  assoc,
  keys
} from 'ramda'

export const getDataFor = key =>
  is(Array)(key) ? path(key) : path(key.split('.'))

export const createProvider = dataProvider => (params = {}) => {
  const dataKey = propOr('data', 'propName', params)
  return {
    Provider: compose(
      setDisplayName('Provider'),
      setPropTypes({
        domain: PropTypes.oneOfType([
          PropTypes.string,
          PropTypes.arrayOf(PropTypes.string)
        ]),
        data: PropTypes.oneOfType([PropTypes.array, PropTypes.object])
      }),
      dataProvider(params),
      withContext(
        {
          get: PropTypes.func.isRequired
        },
        props => ({
          get(key) {
            const data = props[dataKey]
            return either(is(Array), is(String))(key)
              ? { [last(key.split('.'))]: getDataFor(key)(data) }
              : reduce(
                  (acc, propName) =>
                    assoc(propName, getDataFor(key[propName])(data), acc),
                  {},
                  keys(key)
                )
          }
        })
      )
    )(props => props.children),
    withProvider: keys =>
      compose(
        setDisplayName('withProvider'),
        getContext({
          get: PropTypes.func
        }),
        withPropsOnChange(['get'], props => {
          return either(is(Array), is(String))(keys)
            ? {
                [dataKey]: reduce(
                  (acc, key) => mergeDeepRight(props.get(key), acc),
                  {},
                  [].concat(keys)
                )
              }
            : props.get(keys)
        }),
        mapProps(omit(['get']))
      )
  }
}

export const browserStorageProvider = storage => ({ storageKey }) =>
  withProps(props => ({
    data: getDataFor(props.domain)(JSON.parse(getDataFor(storageKey)(storage)))
  }))

export const createProviderWithStorage = storage =>
  createProvider(browserStorageProvider(storage))
