import React, { Component } from 'react'
import './App.css'
import {
  createLocalStorageProvider,
  createSessionStorageProvider,
  createMemoryProvider,
  createReduxProvider
} from './core/createProvider'
import { data } from './boot'

// createReduxProvider
// ---------------------------------------------------------------------------------
// import { Provider as ReduxProvider } from 'react-redux'
// import { connect } from 'react-redux'
// import configureStore from './configureStore'
// import { hget } from './core/hash/selectors'
// const store = configureStore()
// const { Provider: I18nProvider, withProvider } = createReduxProvider({
//   connect: connect((state, { domain }) => ({
//     data: hget('i18n', domain, state)
//   }))
// })
// ---------------------------------------------------------------------------------

// createLocalStorageProvider
// ---------------------------------------------------------------------------------
// const { Provider: I18nProvider, withProvider } = createLocalStorageProvider({
//   storageKey: 'i18n'
// })
// ---------------------------------------------------------------------------------

// createSessionStorageProvider
// ---------------------------------------------------------------------------------
// const { Provider: I18nProvider, withProvider } = createSessionStorageProvider({
//   storageKey: 'i18n'
// })
// ---------------------------------------------------------------------------------

// createMemoryStorageProvider
// ---------------------------------------------------------------------------------
const { Provider: I18nProvider, withProvider } = createMemoryProvider({
  data: data.i18n
})
// ---------------------------------------------------------------------------------

const PresentationHeader = ({ data: { title } }) => (
  <h1 className='App-tittle'>{title}</h1>
)
const Header = withProvider('title')(PresentationHeader)

class App extends Component {
  render() {
    return (
      // Enable when usign createReduxProvider
      // <ReduxProvider store={store}>
      <div>
        <I18nProvider domain='en'>
          <div className='App'>
            <Header />
          </div>
        </I18nProvider>
        <I18nProvider domain='es'>
          <div className='App'>
            <Header />
          </div>
        </I18nProvider>
      </div>
      // </ReduxProvider>
    )
  }
}

export default App
